IMAGE ?= xc32

.PHONY: \
	all \
	xc32 \
	xc32-build \
	xc32-dev

all: xc32

xc32: xc32-build xc32-dev

xc32-build:
	docker build -t $(IMAGE) -f Dockerfile.xc32 .

xc32-dev:
	docker run --rm -it -v $(shell pwd):/code -w /code $(IMAGE)
