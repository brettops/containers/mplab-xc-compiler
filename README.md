# MPLAB XC Compiler containers

[![pipeline status](https://gitlab.com/brettops/containers/mplab-xc-compiler/badges/main/pipeline.svg)](https://gitlab.com/brettops/containers/mplab-xc-compiler/-/commits/main)

Build for PIC microcontrollers with
[MPLAB XC Compiler](https://www.microchip.com/en-us/tools-resources/develop/mplab-xc-compilers).

_Currently only an XC32 image is provided._

## Build & Run

With `make`:

```
make
```

With `docker` commands:

```
docker build -t xc32 -f Dockerfile.xc32 .
docker run --rm -ti xc32
```
